package com.watcharaphon.bmicalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.watcharaphon.bmicalculator.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnCalculate.setOnClickListener { calBMI() }
    }
    private fun calBMI() {
        val txtwegiht = binding.edtWegiht.text.toString()
        val weight = txtwegiht.toDoubleOrNull()
        val txtHegiht = binding.edtHegiht.text.toString()
        val height = txtHegiht.toDoubleOrNull()

        if(weight == null || weight == 0.0 || height == null || height == 0.0) {
            displayBMI(0.0, "")
            return
        }
        var heightPer = height/100
        var bmi = weight / (heightPer * heightPer)
        var result = if (bmi < 18.5){
            "UNDERWEIGHT"
        }else if(bmi <= 24.9){
            "NORMAL"
        }else if(bmi <= 29.9){
            "OVERWEIGHT"
        }else if(bmi <= 34.9){
            "OBESE"
        }else{
            "EXTREMLY OBESE"
        }
        displayBMI(bmi, result)
    }
    private fun displayBMI(bmi: Double, result: String) {
        val formattedBMI = NumberFormat.getNumberInstance().format(bmi)
        binding.txtResult.text = getString(R.string.bmi_resuult, formattedBMI)
        binding.txtResult2.text = result
    }
}